package hello;

import java.util.List;

import eu.specs.negotiation.agreement.offer.AgreementOffer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<AgreementOffer, String> {

    public AgreementOffer findByName(String name);

}
