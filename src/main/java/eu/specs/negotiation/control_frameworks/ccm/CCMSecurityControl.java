
package eu.specs.negotiation.control_frameworks.ccm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for security_controlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="security_controlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_domain" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="security_control" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "security_controlType", namespace = "http://specs-project.main.java.negotiation.eu/schemas/control_frameworks/ccm", propOrder = {
    "controlDescription"
})
public class CCMSecurityControl {

    @XmlElement(name = "control_description", namespace = "http://specs-project.main.java.negotiation.eu/schemas/control_frameworks/ccm", required = true)
    protected String controlDescription;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "control_domain")
    protected String controlDomain;
    @XmlAttribute(name = "security_control")
    protected String securityControl;

    /**
     * Gets the value of the controlDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlDescription() {
        return controlDescription;
    }

    /**
     * Sets the value of the controlDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlDescription(String value) {
        this.controlDescription = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the controlDomain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlDomain() {
        return controlDomain;
    }

    /**
     * Sets the value of the controlDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlDomain(String value) {
        this.controlDomain = value;
    }

    /**
     * Gets the value of the CCMSecurityControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityControl() {
        return securityControl;
    }

    /**
     * Sets the value of the CCMSecurityControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityControl(String value) {
        this.securityControl = value;
    }

}
