
package eu.specs.negotiation.control_frameworks.ccm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityControls_QNAME = new QName("http://specs-project.main.java.negotiation.eu/schemas/control_frameworks/ccm", "security_controls");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CCMSecurityControls }
     * 
     */
    public CCMSecurityControls createSecurityControls() {
        return new CCMSecurityControls();
    }

    /**
     * Create an instance of {@link CCMSecurityControl }
     * 
     */
    public CCMSecurityControl createSecurityControl() {
        return new CCMSecurityControl();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CCMSecurityControls }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.main.java.negotiation.eu/schemas/control_frameworks/ccm", name = "security_controls")
    public JAXBElement<CCMSecurityControls> createSecurityControls(CCMSecurityControls value) {
        return new JAXBElement<CCMSecurityControls>(_SecurityControls_QNAME, CCMSecurityControls.class, null, value);
    }

}
