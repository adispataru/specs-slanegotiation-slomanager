
package eu.specs.negotiation.control_frameworks.nist;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NISTcontrol_frameworkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NISTcontrol_frameworkType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="security_control" type="{http://www.specs-project.eu/schemas/control_frameworks/nist}NISTsecurityControlType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="frameworkName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NISTcontrol_frameworkType", namespace = "http://specs-project.eu/schemas/SLAtemplatecontrol_frameworks/nist", propOrder = {
    "securityControl"
})
public class NISTcontrolFramework {

    @XmlElement(name = "security_control", required = true)
    protected List<NISTsecurityControlType> securityControl;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "frameworkName")
    protected String frameworkName;

    /**
     * Gets the value of the CCMSecurityControl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the CCMSecurityControl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCCMSecurityControl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NISTsecurityControlType }
     * 
     * 
     */
    public List<NISTsecurityControlType> getSecurityControl() {
        if (securityControl == null) {
            securityControl = new ArrayList<NISTsecurityControlType>();
        }
        return this.securityControl;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the frameworkName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrameworkName() {
        return frameworkName;
    }

    /**
     * Sets the value of the frameworkName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrameworkName(String value) {
        this.frameworkName = value;
    }

}
