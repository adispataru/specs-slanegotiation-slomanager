
package eu.specs.negotiation.control_frameworks.nist;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityControls_QNAME = new QName("http://specs-project.eu/schemas/SLAtemplatecontrol_frameworks/nist", "security_controls");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NISTcontrolFramework }
     * 
     */
    public NISTcontrolFramework createNISTcontrolFrameworkType() {
        return new NISTcontrolFramework();
    }

    /**
     * Create an instance of {@link NISTsecurityControlType }
     * 
     */
    public NISTsecurityControlType createNISTsecurityControlType() {
        return new NISTsecurityControlType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NISTcontrolFramework }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.eu/schemas/SLAtemplatecontrol_frameworks/nist", name = "security_controls")
    public JAXBElement<NISTcontrolFramework> createSecurityControls(NISTcontrolFramework value) {
        return new JAXBElement<NISTcontrolFramework>(_SecurityControls_QNAME, NISTcontrolFramework.class, null, value);
    }

}
