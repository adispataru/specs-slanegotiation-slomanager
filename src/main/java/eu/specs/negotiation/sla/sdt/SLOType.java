
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SLOType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SLOType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MetricREF" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SLOexpression" type="{http://specs-project.eu/schemas/SLAtemplate}SLOexpressionType"/>
 *         &lt;element name="importance_weight" type="{http://specs-project.eu/schemas/SLAtemplate}weightType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="SLO_ID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SLOType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "metricREF",
    "slOexpression",
    "importanceWeight"
})
public class SLOType {

    @XmlElement(name = "MetricREF", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected String metricREF;
    @XmlElement(name = "SLOexpression", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected SLOexpressionType slOexpression;
    @XmlElement(name = "importance_weight", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected WeightType importanceWeight;
    @XmlAttribute(name = "SLO_ID", required = true)
    protected String sloid;

    /**
     * Gets the value of the metricREF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetricREF() {
        return metricREF;
    }

    /**
     * Sets the value of the metricREF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetricREF(String value) {
        this.metricREF = value;
    }

    /**
     * Gets the value of the slOexpression property.
     * 
     * @return
     *     possible object is
     *     {@link SLOexpressionType }
     *     
     */
    public SLOexpressionType getSLOexpression() {
        return slOexpression;
    }

    /**
     * Sets the value of the slOexpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link SLOexpressionType }
     *     
     */
    public void setSLOexpression(SLOexpressionType value) {
        this.slOexpression = value;
    }

    /**
     * Gets the value of the importanceWeight property.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getImportanceWeight() {
        return importanceWeight;
    }

    /**
     * Sets the value of the importanceWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setImportanceWeight(WeightType value) {
        this.importanceWeight = value;
    }

    /**
     * Gets the value of the sloid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSLOID() {
        return sloid;
    }

    /**
     * Sets the value of the sloid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSLOID(String value) {
        this.sloid = value;
    }

}
