
package eu.specs.negotiation.sla.sdt;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for capabilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="capabilityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_framework">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="CCMSecurityControl" type="{http://specs-project.eu/schemas/SLAtemplate}NISTcontrol" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="frameworkName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="descr" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "capability", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "controlFramework"
})
@XmlRootElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", name = "capability")
public class CapabilityType {

    @XmlElement(name = "controlFramework", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected CapabilityType.ControlFramework controlFramework;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "description", required = true)
    protected String description;

    /**
     * Gets the value of the controlFramework property.
     * 
     * @return
     *     possible object is
     *     {@link CapabilityType.ControlFramework }
     *     
     */
    public CapabilityType.ControlFramework getControlFramework() {
        return controlFramework;
    }

    /**
     * Sets the value of the controlFramework property.
     * 
     * @param value
     *     allowed object is
     *     {@link CapabilityType.ControlFramework }
     *     
     */
    public void setControlFramework(CapabilityType.ControlFramework value) {
        this.controlFramework = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="CCMSecurityControl" type="{http://specs-project.eu/schemas/SLAtemplate}NISTcontrol" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="frameworkName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "securityControl"
    })
    public static class ControlFramework {

        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        protected List<NISTcontrol> securityControl;
        @XmlAttribute(name = "id", required = true)
        protected String id;
        @XmlAttribute(name = "frameworkName", required = true)
        protected String frameworkName;


        /**
         * Gets the value of the CCMSecurityControl property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the CCMSecurityControl property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCCMSecurityControl().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NISTcontrol }
         * 
         * 
         */
        public List<NISTcontrol> getSecurityControl() {
            if (securityControl == null) {
                securityControl = new ArrayList<NISTcontrol>();
            }
            return this.securityControl;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the frameworkName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFrameworkName() {
            return frameworkName;
        }

        /**
         * Sets the value of the frameworkName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFrameworkName(String value) {
            this.frameworkName = value;
        }

    }

}
