
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for NISTcontrol complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NISTcontrol">
 *   &lt;complexContent>
 *     &lt;extension base="{http://specs-project.eu/schemas/SLAtemplate}NISTsecurityControlType">
 *       &lt;sequence>
 *         &lt;element name="importance_weight" type="{http://specs-project.eu/schemas/SLAtemplate}weightType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NISTcontrol", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "importanceWeight"
})
@XmlRootElement(name = "securityControl", namespace = "http://specs-project.eu/schemas/SLAtemplate")
public class NISTcontrol
    extends NISTsecurityControlType
{

    @XmlElement(name = "importance_weight", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    @XmlSchemaType(name = "string")
    protected WeightType importanceWeight;

    /**
     * Gets the value of the importanceWeight property.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getImportanceWeight() {
        return importanceWeight;
    }

    /**
     * Sets the value of the importanceWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setImportanceWeight(WeightType value) {
        this.importanceWeight = value;
    }

}
