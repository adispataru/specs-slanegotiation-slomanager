
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for SLOexpressionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SLOexpressionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="oneOpExpression">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="operator" type="{http://specs-project.eu/schemas/SLAtemplate}oneOpOperator"/>
 *                   &lt;element name="operand" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="twoOpExpression">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="operator" type="{http://specs-project.eu/schemas/SLAtemplate}twoOpOperator"/>
 *                   &lt;element name="operand1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *                   &lt;element name="operand2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SLOexpressionType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "oneOpExpression",
    "twoOpExpression"
})
public class SLOexpressionType {

    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected SLOexpressionType.OneOpExpression oneOpExpression;
    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected SLOexpressionType.TwoOpExpression twoOpExpression;

    /**
     * Gets the value of the oneOpExpression property.
     * 
     * @return
     *     possible object is
     *     {@link SLOexpressionType.OneOpExpression }
     *     
     */
    public SLOexpressionType.OneOpExpression getOneOpExpression() {
        return oneOpExpression;
    }

    /**
     * Sets the value of the oneOpExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link SLOexpressionType.OneOpExpression }
     *     
     */
    public void setOneOpExpression(SLOexpressionType.OneOpExpression value) {
        this.oneOpExpression = value;
    }

    /**
     * Gets the value of the twoOpExpression property.
     * 
     * @return
     *     possible object is
     *     {@link SLOexpressionType.TwoOpExpression }
     *     
     */
    public SLOexpressionType.TwoOpExpression getTwoOpExpression() {
        return twoOpExpression;
    }

    /**
     * Sets the value of the twoOpExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link SLOexpressionType.TwoOpExpression }
     *     
     */
    public void setTwoOpExpression(SLOexpressionType.TwoOpExpression value) {
        this.twoOpExpression = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="operator" type="{http://specs-project.eu/schemas/SLAtemplate}oneOpOperator"/>
     *         &lt;element name="operand" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operator",
        "operand"
    })
    public static class OneOpExpression {

        @XmlAttribute(required = true)
        @XmlSchemaType(name = "string")
        protected OneOpOperator operator;
        @XmlAttribute(required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected Object operand;

        /**
         * Gets the value of the operator property.
         * 
         * @return
         *     possible object is
         *     {@link OneOpOperator }
         *     
         */
        public OneOpOperator getOperator() {
            return operator;
        }

        /**
         * Sets the value of the operator property.
         * 
         * @param value
         *     allowed object is
         *     {@link OneOpOperator }
         *     
         */
        public void setOperator(OneOpOperator value) {
            this.operator = value;
        }

        /**
         * Gets the value of the operand property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getOperand() {
            return operand;
        }

        /**
         * Sets the value of the operand property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setOperand(Object value) {
            this.operand = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="operator" type="{http://specs-project.eu/schemas/SLAtemplate}twoOpOperator"/>
     *         &lt;element name="operand1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
     *         &lt;element name="operand2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operator",
        "operand1",
        "operand2"
    })
    public static class TwoOpExpression {

        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        @XmlSchemaType(name = "string")
        protected TwoOpOperator operator;
        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected Object operand1;
        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected Object operand2;

        /**
         * Gets the value of the operator property.
         * 
         * @return
         *     possible object is
         *     {@link TwoOpOperator }
         *     
         */
        public TwoOpOperator getOperator() {
            return operator;
        }

        /**
         * Sets the value of the operator property.
         * 
         * @param value
         *     allowed object is
         *     {@link TwoOpOperator }
         *     
         */
        public void setOperator(TwoOpOperator value) {
            this.operator = value;
        }

        /**
         * Gets the value of the operand1 property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getOperand1() {
            return operand1;
        }

        /**
         * Sets the value of the operand1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setOperand1(Object value) {
            this.operand1 = value;
        }

        /**
         * Gets the value of the operand2 property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getOperand2() {
            return operand2;
        }

        /**
         * Sets the value of the operand2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setOperand2(Object value) {
            this.operand2 = value;
        }

    }

}
