
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for twoOpOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="twoOpOperator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="in excluded"/>
 *     &lt;enumeration value="in included"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "twoOpOperator", namespace = "http://specs-project.eu/schemas/SLAtemplate")
@XmlEnum
public enum TwoOpOperator {

    @XmlEnumValue("in excluded")
    IN_EXCLUDED("in excluded"),
    @XmlEnumValue("in included")
    IN_INCLUDED("in included");
    private final String value;

    TwoOpOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TwoOpOperator fromValue(String v) {
        for (TwoOpOperator c: TwoOpOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
