
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.sla package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Endpoint_QNAME = new QName("http://specs-project.eu/schemas/SLAtemplate", "endpoint");
    private final static QName _ServiceDescription_QNAME = new QName("http://specs-project.eu/schemas/SLAtemplate", "serviceDescription");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.sla
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CapabilityType }
     * 
     */
    public CapabilityType createCapabilityType() {
        return new CapabilityType();
    }

    /**
     * Create an instance of {@link SecurityMetricType }
     * 
     */
    public SecurityMetricType createSecurityMetricType() {
        return new SecurityMetricType();
    }

    /**
     * Create an instance of {@link SecurityMetricType.MetricParameters }
     * 
     */
    public SecurityMetricType.MetricParameters createSecurityMetricTypeMetricParameters() {
        return new SecurityMetricType.MetricParameters();
    }

    /**
     * Create an instance of {@link SecurityMetricType.MetricRules }
     * 
     */
    public SecurityMetricType.MetricRules createSecurityMetricTypeMetricRules() {
        return new SecurityMetricType.MetricRules();
    }

    /**
     * Create an instance of {@link SLOexpressionType }
     * 
     */
    public SLOexpressionType createSLOexpressionType() {
        return new SLOexpressionType();
    }


    /**
     * Create an instance of {@link ServiceDescriptionType }
     * 
     */
    public ServiceDescriptionType createServiceDescriptionType() {
        return new ServiceDescriptionType();
    }

    /**
     * Create an instance of {@link ServiceDescriptionType.ServiceResources }
     * 
     */
    public ServiceDescriptionType.ServiceResources createServiceDescriptionTypeServiceResources() {
        return new ServiceDescriptionType.ServiceResources();
    }

    /**
     * Create an instance of {@link ServiceDescriptionType.ServiceResources.ResourcesProvider }
     * 
     */
    public ServiceDescriptionType.ServiceResources.ResourcesProvider createServiceDescriptionTypeServiceResourcesResourcesProvider() {
        return new ServiceDescriptionType.ServiceResources.ResourcesProvider();
    }

    /**
     * Create an instance of {@link ObjectiveList }
     * 
     */
    public ObjectiveList createObjectiveList() {
        return new ObjectiveList();
    }

    /**
     * Create an instance of {@link SLOType }
     * 
     */
    public SLOType createSLOType() {
        return new SLOType();
    }

    /**
     * Create an instance of {@link NISTcontrol }
     * 
     */
    public NISTcontrol createNISTcontrol() {
        return new NISTcontrol();
    }

    /**
     * Create an instance of {@link NISTsecurityControlType }
     * 
     */
    public NISTsecurityControlType createNISTsecurityControlType() {
        return new NISTsecurityControlType();
    }


    /**
     * Create an instance of {@link CapabilityType.ControlFramework }
     * 
     */
    public CapabilityType.ControlFramework createCapabilityTypeControlFramework() {
        return new CapabilityType.ControlFramework();
    }

    /**
     * Create an instance of {@link SecurityMetricType.MetricParameters.MetricParameter }
     * 
     */
    public SecurityMetricType.MetricParameters.MetricParameter createSecurityMetricTypeMetricParametersMetricParameter() {
        return new SecurityMetricType.MetricParameters.MetricParameter();
    }

    /**
     * Create an instance of {@link SecurityMetricType.MetricRules.MetricRule }
     * 
     */
    public SecurityMetricType.MetricRules.MetricRule createSecurityMetricTypeMetricRulesMetricRule() {
        return new SecurityMetricType.MetricRules.MetricRule();
    }

    /**
     * Create an instance of {@link SLOexpressionType.OneOpExpression }
     * 
     */
    public SLOexpressionType.OneOpExpression createSLOexpressionTypeOneOpExpression() {
        return new SLOexpressionType.OneOpExpression();
    }

    /**
     * Create an instance of {@link SLOexpressionType.TwoOpExpression }
     * 
     */
    public SLOexpressionType.TwoOpExpression createSLOexpressionTypeTwoOpExpression() {
        return new SLOexpressionType.TwoOpExpression();
    }



    /**
     * Create an instance of {@link ServiceDescriptionType.Capabilities }
     * 
     */
    public ServiceDescriptionType.Capabilities createServiceDescriptionTypeCapabilities() {
        return new ServiceDescriptionType.Capabilities();
    }

    /**
     * Create an instance of {@link ServiceDescriptionType.SecurityMetrics }
     * 
     */
    public ServiceDescriptionType.SecurityMetrics createServiceDescriptionTypeSecurityMetrics() {
        return new ServiceDescriptionType.SecurityMetrics();
    }

    /**
     * Create an instance of {@link ServiceDescriptionType.ServiceResources.ResourcesProvider.VM }
     * 
     */
    public ServiceDescriptionType.ServiceResources.ResourcesProvider.VM createServiceDescriptionTypeServiceResourcesResourcesProviderVM() {
        return new ServiceDescriptionType.ServiceResources.ResourcesProvider.VM();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.eu/schemas/SLAtemplate", name = "endpoint")
    public JAXBElement<String> createEndpoint(String value) {
        return new JAXBElement<String>(_Endpoint_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceDescriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.eu/schemas/SLAtemplate", name = "serviceDescription")
    public JAXBElement<ServiceDescriptionType> createServiceDescription(ServiceDescriptionType value) {
        return new JAXBElement<ServiceDescriptionType>(_ServiceDescription_QNAME, ServiceDescriptionType.class, null, value);
    }

}
