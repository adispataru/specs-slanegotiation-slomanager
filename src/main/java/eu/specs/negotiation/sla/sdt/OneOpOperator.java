
package eu.specs.negotiation.sla.sdt;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for oneOpOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="oneOpOperator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="equal"/>
 *     &lt;enumeration value="greater then"/>
 *     &lt;enumeration value="less then"/>
 *     &lt;enumeration value="greater then or equal"/>
 *     &lt;enumeration value="less then or equal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "oneOpOperator", namespace = "http://specs-project.eu/schemas/SLAtemplate")
@XmlEnum
public enum OneOpOperator {

    @XmlEnumValue("equal")
    EQUAL("equal"),
    @XmlEnumValue("greater then")
    GREATER_THEN("greater then"),
    @XmlEnumValue("less then")
    LESS_THEN("less then"),
    @XmlEnumValue("greater then or equal")
    GREATER_THEN_OR_EQUAL("greater then or equal"),
    @XmlEnumValue("less then or equal")
    LESS_THEN_OR_EQUAL("less then or equal");
    private final String value;

    OneOpOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OneOpOperator fromValue(String v) {
        for (OneOpOperator c: OneOpOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
