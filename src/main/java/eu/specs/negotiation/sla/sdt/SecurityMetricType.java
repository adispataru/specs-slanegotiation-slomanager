
package eu.specs.negotiation.sla.sdt;

import eu.specs.negotiation.metrics.AbstractMetricType;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for security_metricType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="security_metricType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="metricDefinition" type="{http://specs-project.eu/schemas/SLAtemplate}AbstractMetricDefinition"/>
 *         &lt;element name="metricRules" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="metricRule" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ruleDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ruleValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="ruleId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="metricParameters" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="metricParameter" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="paramDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="paramType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="integer"/>
 *                                   &lt;enumeration value="float"/>
 *                                   &lt;enumeration value="string"/>
 *                                   &lt;enumeration value="list"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="paramValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="paramId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="referenceId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "metricDefinition",
    "metricRules",
    "metricParameters"
})
@XmlRootElement(name = "SecurityMetric")
public class SecurityMetricType {

    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true, name = "MetricDefinition")
    protected AbstractMetricType.AbstractMetricDefinition metricDefinition;
//    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true, name = "AbstractMetricRuleDefinition")
//    protected AbstractMetricType.AbstractMetricRuleDefinition metricRuleDefinition;
//    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true, name = "AbstractMetricParameterDefinition")
//    protected AbstractMetricType.AbstractMetricParameterDefinition metricParameterDefinition;


    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected List<SecurityMetricType.MetricRules> metricRules;
    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected List<SecurityMetricType.MetricParameters> metricParameters;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "referenceId", required = true)
    protected String referenceId;


    /**
     * Gets the value of the metricDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractMetricDefinition }
     *     
     */
    public AbstractMetricType.AbstractMetricDefinition getMetricDefinition() {
        return metricDefinition;
    }

    /**
     * Sets the value of the metricDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractMetricDefinition }
     *     
     */
    public void setMetricDefinition(AbstractMetricType.AbstractMetricDefinition value) {
        this.metricDefinition = value;
    }

    /**
     * Gets the value of the metricRules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metricRules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetricRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityMetricType.MetricRules }
     * 
     * 
     */
    public List<SecurityMetricType.MetricRules> getMetricRules() {
        if (metricRules == null) {
            metricRules = new ArrayList<SecurityMetricType.MetricRules>();
        }
        return this.metricRules;
    }

    /**
     * Gets the value of the metricParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metricParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetricParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityMetricType.MetricParameters }
     * 
     * 
     */
    public List<SecurityMetricType.MetricParameters> getMetricParameters() {
        if (metricParameters == null) {
            metricParameters = new ArrayList<SecurityMetricType.MetricParameters>();
        }
        return this.metricParameters;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the referenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Sets the value of the referenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceId(String value) {
        this.referenceId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="metricParameter" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="paramDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="paramType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="integer"/>
     *                         &lt;enumeration value="float"/>
     *                         &lt;enumeration value="string"/>
     *                         &lt;enumeration value="list"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="paramValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="paramId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "MetricParameters", propOrder = {
        "metricParameter"
    })
    public static class MetricParameters {

        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
        protected List<SecurityMetricType.MetricParameters.MetricParameter> metricParameter;

        /**
         * Gets the value of the metricParameter property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the metricParameter property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMetricParameter().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurityMetricType.MetricParameters.MetricParameter }
         * 
         * 
         */
        public List<SecurityMetricType.MetricParameters.MetricParameter> getMetricParameter() {
            if (metricParameter == null) {
                metricParameter = new ArrayList<SecurityMetricType.MetricParameters.MetricParameter>();
            }
            return this.metricParameter;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="paramDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="paramType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="integer"/>
         *               &lt;enumeration value="float"/>
         *               &lt;enumeration value="string"/>
         *               &lt;enumeration value="list"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="paramValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *       &lt;attribute name="paramId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "MetricParameter", propOrder = {
            "paramDescription",
            "paramType",
            "paramValue"
        })
        public static class MetricParameter {

            @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
            protected String paramDescription;
            @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
            protected String paramType;
            @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
            protected String paramValue;
            @XmlAttribute(name = "paramId", required = true)
            protected String paramId;

            /**
             * Gets the value of the paramDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamDescription() {
                return paramDescription;
            }

            /**
             * Sets the value of the paramDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamDescription(String value) {
                this.paramDescription = value;
            }

            /**
             * Gets the value of the paramType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamType() {
                return paramType;
            }

            /**
             * Sets the value of the paramType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamType(String value) {
                this.paramType = value;
            }

            /**
             * Gets the value of the paramValue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamValue() {
                return paramValue;
            }

            /**
             * Sets the value of the paramValue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamValue(String value) {
                this.paramValue = value;
            }

            /**
             * Gets the value of the paramId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamId() {
                return paramId;
            }

            /**
             * Sets the value of the paramId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamId(String value) {
                this.paramId = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="metricRule" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ruleDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ruleValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="ruleId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "MetricRules", propOrder = {
        "metricRule"
    })
    public static class MetricRules {

        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
        protected List<SecurityMetricType.MetricRules.MetricRule> metricRule;

        /**
         * Gets the value of the metricRule property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the metricRule property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMetricRule().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurityMetricType.MetricRules.MetricRule }
         * 
         * 
         */
        public List<SecurityMetricType.MetricRules.MetricRule> getMetricRule() {
            if (metricRule == null) {
                metricRule = new ArrayList<SecurityMetricType.MetricRules.MetricRule>();
            }
            return this.metricRule;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ruleDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ruleValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *       &lt;attribute name="ruleId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ruleDescription",
            "ruleValue"
        })
        public static class MetricRule {

            @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
            protected String ruleDescription;
            @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
            protected String ruleValue;
            @XmlAttribute(name = "ruleId", required = true)
            protected String ruleId;

            /**
             * Gets the value of the ruleDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleDescription() {
                return ruleDescription;
            }

            /**
             * Sets the value of the ruleDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleDescription(String value) {
                this.ruleDescription = value;
            }

            /**
             * Gets the value of the ruleValue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleValue() {
                return ruleValue;
            }

            /**
             * Sets the value of the ruleValue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleValue(String value) {
                this.ruleValue = value;
            }

            /**
             * Gets the value of the ruleId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleId() {
                return ruleId;
            }

            /**
             * Sets the value of the ruleId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleId(String value) {
                this.ruleId = value;
            }

        }

    }

}
