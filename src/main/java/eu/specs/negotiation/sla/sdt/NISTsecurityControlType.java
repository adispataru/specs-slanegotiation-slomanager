
package eu.specs.negotiation.sla.sdt;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NISTsecurityControlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NISTsecurityControlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_family_id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_family_name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="security_control" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="control_enhancement" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NISTsecurityControlType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "controlDescription"
})
@XmlSeeAlso({
    NISTcontrol.class
})
public class NISTsecurityControlType {

    @XmlElement(name = "description", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected String controlDescription;
    @XmlAttribute(name = "id", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected String id;
    @XmlAttribute(name = "name", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected String name;
    @XmlAttribute(name = "control_family", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected String controlFamily;
    @XmlAttribute(name = "control_family_name", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected String controlFamilyName;
    @XmlAttribute(name = "securityControl", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected BigInteger securityControl;
    @XmlAttribute(name = "control_enhancement", required = true, namespace = "http://specs-project.eu/schemas/nist")
    protected BigInteger controlEnhancement;

    /**
     * Gets the value of the controlDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlDescription() {
        return controlDescription;
    }

    /**
     * Sets the value of the controlDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlDescription(String value) {
        this.controlDescription = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the controlFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlFamily() {
        return controlFamily;
    }

    /**
     * Sets the value of the controlFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlFamily(String value) {
        this.controlFamily = value;
    }

    /**
     * Gets the value of the controlFamilyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlFamilyName() {
        return controlFamilyName;
    }

    /**
     * Sets the value of the controlFamilyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlFamilyName(String value) {
        this.controlFamilyName = value;
    }

    /**
     * Gets the value of the CCMSecurityControl property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSecurityControl() {
        return securityControl;
    }

    /**
     * Sets the value of the CCMSecurityControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSecurityControl(BigInteger value) {
        this.securityControl = value;
    }

    /**
     * Gets the value of the controlEnhancement property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getControlEnhancement() {
        return controlEnhancement;
    }

    /**
     * Sets the value of the controlEnhancement property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setControlEnhancement(BigInteger value) {
        this.controlEnhancement = value;
    }

}
