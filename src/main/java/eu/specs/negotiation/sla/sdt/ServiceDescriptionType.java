
package eu.specs.negotiation.sla.sdt;

import eu.specs.negotiation.metrics.AbstractMetricType;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for serviceDescriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="serviceDescriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceResources" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resourcesProvider" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="capabilities">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="capability" type="{http://specs-project.eu/schemas/SLAtemplate}capabilityType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="security_metrics">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="security_metric" type="{http://specs-project.eu/schemas/SLAtemplate}security_metricType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceDescriptionType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "serviceResources",
    "capabilities",
    "securityMetrics"
})
@XmlRootElement
public class ServiceDescriptionType {

    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected List<ServiceDescriptionType.ServiceResources> serviceResources;
    @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    //@XmlElementRef(namespace = "http://specs-project.eu/schemas/SLAtemplate")
    protected Capabilities capabilities;
    @XmlElement(name = "security_metrics", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
    protected SecurityMetrics securityMetrics;

    /**
     * Gets the value of the serviceResources property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceResources property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceResources().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceDescriptionType.ServiceResources }
     * 
     * 
     */
    public List<ServiceDescriptionType.ServiceResources> getServiceResources() {
        if (serviceResources == null) {
            serviceResources = new ArrayList<ServiceDescriptionType.ServiceResources>();
        }
        return this.serviceResources;
    }

    /**
     * Gets the value of the capabilities property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceDescriptionType.Capabilities }
     *     
     */
    public Capabilities getCapabilities() {
        return capabilities;
    }

    /**
     * Sets the value of the capabilities property.
     *
     * @param value
     *     allowed object is
     *     {@link Capabilities }
     *     
     */
    public void setCapabilities(Capabilities value) {
        this.capabilities = value;
    }

    /**
     * Gets the value of the securityMetrics property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceDescriptionType.SecurityMetrics }
     *     
     */
    public SecurityMetrics getSecurityMetrics() {
        return securityMetrics;
    }

    /**
     * Sets the value of the securityMetrics property.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityMetrics }
     *     
     */
    public void setSecurityMetrics(SecurityMetrics value) {
        this.securityMetrics = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="capability" type="{http://specs-project.eu/schemas/SLAtemplate}capabilityType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "capabilities", propOrder = {
        "capability"
    }, namespace = "http://specs-project.eu/schemas/SLAtemplate")
    @XmlRootElement
    public static class Capabilities {

        //@XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        @XmlElementRef(name = "capabilities", namespace = "http://specs-project.eu/schemas/SLAtemplate")
        protected List<CapabilityType> capability;

        /**
         * Gets the value of the capability property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the capability property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCapability().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CapabilityType }
         * 
         * 
         */
        public List<CapabilityType> getCapability() {
            if (capability == null) {
                capability = new ArrayList<CapabilityType>();
            }
            return this.capability;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="security_metric" type="{http://specs-project.eu/schemas/SLAtemplate}security_metricType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "securityMetric"
    }, namespace = "http://specs-project.eu/schemas/SLAtemplate")
    @XmlRootElement(name = "security_metrics")
    public static class SecurityMetrics {

        @XmlElement(name = "Metric", namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
        protected List<AbstractMetricType> securityMetric;

        /**
         * Gets the value of the securityMetric property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the securityMetric property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSecurityMetric().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurityMetricType }
         * 
         * 
         */
        public List<AbstractMetricType> getSecurityMetric() {
            if (securityMetric == null) {
                securityMetric = new ArrayList<AbstractMetricType>();
            }
            return this.securityMetric;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resourcesProvider" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resourcesProvider"
    }, namespace = "http://specs-project.eu/schemas/SLAtemplate")
    @XmlRootElement
    public static class ServiceResources {

        @XmlElement(namespace = "http://specs-project.eu/schemas/SLAtemplate", required = true)
      //  @XmlElementRef(name = "resourcesProvider", namespace = "http://specs-project.eu/schemas/SLAtemplate")
        protected List<ServiceDescriptionType.ServiceResources.ResourcesProvider> resourcesProvider;

        /**
         * Gets the value of the resourcesProvider property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resourcesProvider property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResourcesProvider().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ServiceDescriptionType.ServiceResources.ResourcesProvider }
         * 
         * 
         */
        public List<ServiceDescriptionType.ServiceResources.ResourcesProvider> getResourcesProvider() {
            if (resourcesProvider == null) {
                resourcesProvider = new ArrayList<ServiceDescriptionType.ServiceResources.ResourcesProvider>();
            }
            return this.resourcesProvider;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "resourcesProvider", propOrder = {
            "id","name", "zone", "maxAllowedVMs", "vm"
        }, namespace = "http://specs-project.eu/schemas/SLAtemplate")
        @XmlRootElement
        public static class ResourcesProvider {

            @XmlElement(name = "VM", namespace = "http://specs-project.eu/schemas/SLAtemplate")
            protected List<ServiceDescriptionType.ServiceResources.ResourcesProvider.VM> vm;
            @XmlAttribute(name = "id", required = true)
            protected String id;
            @XmlAttribute(name = "name", required = true)
            protected String name;
            @XmlAttribute(name = "zone")
            protected String zone;
            @XmlAttribute(name = "maxAllowedVMs")
            protected String maxAllowedVMs;

            public String getMaxAllowedVMs() {
                return maxAllowedVMs;
            }

            public void setMaxAllowedVMs(String maxAllowdVMs) {
                this.maxAllowedVMs = maxAllowdVMs;
            }

            /**
             * Gets the value of the vm property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the vm property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getVM().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ServiceDescriptionType.ServiceResources.ResourcesProvider.VM }
             * 
             * 
             */
            public List<ServiceDescriptionType.ServiceResources.ResourcesProvider.VM> getVM() {
                if (vm == null) {
                    vm = new ArrayList<ServiceDescriptionType.ServiceResources.ResourcesProvider.VM>();
                }
                return this.vm;
            }

            /**
             * Gets the value of the id property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Sets the value of the id property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the zone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZone() {
                return zone;
            }

            /**
             * Sets the value of the zone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZone(String value) {
                this.zone = value;
            }



            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlRootElement
            public static class VM {

                @XmlAttribute(name = "appliance")
                protected String appliance;
                @XmlAttribute(name = "hardware")
                protected String hardware;
                @XmlAttribute(name = "descr")
                protected String descr;

                /**
                 * Gets the value of the appliance property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAppliance() {
                    return appliance;
                }

                /**
                 * Sets the value of the appliance property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAppliance(String value) {
                    this.appliance = value;
                }

                /**
                 * Gets the value of the hardware property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHardware() {
                    return hardware;
                }

                /**
                 * Sets the value of the hardware property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHardware(String value) {
                    this.hardware = value;
                }

                /**
                 * Gets the value of the descr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescr() {
                    return descr;
                }

                /**
                 * Sets the value of the descr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescr(String value) {
                    this.descr = value;
                }

            }

        }

    }

}
