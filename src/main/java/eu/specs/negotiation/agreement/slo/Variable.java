package eu.specs.negotiation.agreement.slo;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "name", "metric", "location"
})
@XmlRootElement(name = "Variable", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class Variable {


    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "Metric", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String metric;
    @XmlElement(name = "Location", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String location;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

