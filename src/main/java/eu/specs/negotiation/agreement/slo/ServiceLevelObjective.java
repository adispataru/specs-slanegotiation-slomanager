package eu.specs.negotiation.agreement.slo;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "customServiceLevel",
})
@XmlRootElement(name = "ServiceLevelObjective", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class ServiceLevelObjective {

        @XmlElement(name = "CustomServiceLevel", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
        private CustomServiceLevel customServiceLevel;

        public CustomServiceLevel getCustomServiceLevel() {
                return customServiceLevel;
        }

        public void setCustomServiceLevel(CustomServiceLevel customServiceLevel) {
                this.customServiceLevel = customServiceLevel;
        }
}
