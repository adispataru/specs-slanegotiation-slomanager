package eu.specs.negotiation.agreement.slo;


import eu.specs.negotiation.sla.sdt.ObjectiveList;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectiveList",
})
@XmlRootElement(name = "CustomServiceLevel", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class CustomServiceLevel {

        @XmlElement(name = "objectiveList",  namespace = "http://specs-project.eu/schemas/SLAtemplate")
        private ObjectiveList objectiveList;

        public ObjectiveList getObjectiveList() {
                return objectiveList;
        }

        public void setObjectiveList(ObjectiveList objectiveList) {
                this.objectiveList = objectiveList;
        }
}
