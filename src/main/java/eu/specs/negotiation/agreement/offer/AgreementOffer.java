package eu.specs.negotiation.agreement.offer;


import eu.specs.negotiation.agreement.Context;
import eu.specs.negotiation.agreement.terms.Terms;
import org.springframework.data.annotation.Id;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "name", "context", "terms"
})

@XmlRootElement(name = "AgreementOffer", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class AgreementOffer {

    @XmlElement(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    @Id
    private String name;

    @XmlElement(name = "Context", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private Context context;

    @XmlElement(name = "Terms", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private Terms terms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }
}
