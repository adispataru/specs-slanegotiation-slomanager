package eu.specs.negotiation.agreement;

import javax.xml.bind.annotation.*;
import java.util.Date;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "agreementInitiator", "agreementResponder",
        "serviceProvider", "expirationTime", "templateName"
})
@XmlRootElement(name = "Context", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class Context {
    @XmlElement(name = "AgreementInitiator", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String agreementInitiator;
    @XmlElement(name = "AgreementResponder", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String agreementResponder;
    @XmlElement(name = "ServiceProvider", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String serviceProvider;
    @XmlElement(name = "ExpirationTime", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private Date expirationTime;
    @XmlElement(name = "TemplateName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String templateName;

    public String getAgreementInitiator() {
        return agreementInitiator;
    }

    public void setAgreementInitiator(String agreementInitiator) {
        this.agreementInitiator = agreementInitiator;
    }

    public String getAgreementResponder() {
        return agreementResponder;
    }

    public void setAgreementResponder(String agreementResponder) {
        this.agreementResponder = agreementResponder;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}
