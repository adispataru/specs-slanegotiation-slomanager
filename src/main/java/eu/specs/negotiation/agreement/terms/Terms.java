package eu.specs.negotiation.agreement.terms;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "all",
})
@XmlRootElement(name = "Terms")
public class Terms {

    //@XmlElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    @XmlElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private All all;

    public All getAll() {

        return all;
    }

    public void setAll(All all) {
        this.all = all;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(propOrder = {"all",})
    @XmlRootElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    public static class All{
        @XmlElementRef(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
        private List<Term> all;

        public List<Term> getAll() {
            if(all == null)
                all = new ArrayList<Term>();
            return all;
        }

        public void setAll(List<Term> all) {
            this.all = all;
        }
    }
}
