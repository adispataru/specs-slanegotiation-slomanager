package eu.specs.negotiation.agreement.terms;

import eu.specs.negotiation.sla.sdt.ServiceDescriptionType;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "serviceName",
        "serviceDescription",
})
@XmlRootElement(name = "ServiceDescriptionTerm", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class ServiceDescriptionTerm extends Term{
    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "ServiceName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String serviceName;
    @XmlElement(name = "serviceDescription", namespace = "http://specs-project.eu/schemas/SLAtemplate")
    private ServiceDescriptionType serviceDescription;

    public ServiceDescriptionType getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(ServiceDescriptionType serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
