package eu.specs.negotiation.agreement.terms;



import eu.specs.negotiation.agreement.slo.ServiceLevelObjective;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "serviceScope", "qualifyingCondition",
        "name", "obligated", "serviceLevelObjective"
})
@XmlRootElement(name = "GuaranteeTerm", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class GuaranteeTerm extends Term{

    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "Obligated", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String obligated;
    @XmlElement(name = "ServiceScope", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private ServiceScope serviceScope;
    @XmlElement(name = "QualifyingCondition", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private boolean qualifyingCondition;
    @XmlElement(name = "ServiceLevelObjective", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private ServiceLevelObjective serviceLevelObjective;


    public boolean isQualifyingCondition() {
        return qualifyingCondition;
    }

    public void setQualifyingCondition(boolean qualifyingCondition) {
        this.qualifyingCondition = qualifyingCondition;
    }

    public ServiceScope getServiceScope() {
        return serviceScope;
    }

    public void setServiceScope(ServiceScope serviceScope) {
        this.serviceScope = serviceScope;
    }

    public ServiceLevelObjective getServiceLevelObjective() {
        return serviceLevelObjective;
    }

    public void setServiceLevelObjective(ServiceLevelObjective serviceLevelObjective) {
        this.serviceLevelObjective = serviceLevelObjective;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObligated() {
        return obligated;
    }

    public void setObligated(String obligated) {
        this.obligated = obligated;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "serviceName",
    })
    @XmlRootElement(name = "wsag:ServiceScope")
    public static class ServiceScope {
        @XmlAttribute(name = "wsag:ServiceName")
        private String serviceName;

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }
    }



}
