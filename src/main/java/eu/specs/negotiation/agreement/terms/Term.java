package eu.specs.negotiation.agreement.terms;

import eu.specs.negotiation.agreement.slo.ServiceProperties;

import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlSeeAlso({ServiceDescriptionTerm.class, GuaranteeTerm.class, ServiceProperties.class})
public abstract class Term {


}
