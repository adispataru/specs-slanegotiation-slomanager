
package eu.specs.negotiation.metrics;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for security_metricsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="security_metricsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AbstractMetric" type="{http://www.specs-project.eu/schemas/metrics}AbstractMetricType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Metric" type="{http://www.specs-project.eu/schemas/metrics}MetricType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "security_metricsType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "abstractMetric",
    "metric"
})
public class SecurityMetricsType {

    @XmlElement(name = "AbstractMetric")
    protected List<AbstractMetricType> abstractMetric;
    @XmlElement(name = "Metric")
    protected List<MetricType> metric;

    /**
     * Gets the value of the abstractMetric property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the abstractMetric property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAbstractMetric().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractMetricType }
     * 
     * 
     */
    public List<AbstractMetricType> getAbstractMetric() {
        if (abstractMetric == null) {
            abstractMetric = new ArrayList<AbstractMetricType>();
        }
        return this.abstractMetric;
    }

    /**
     * Gets the value of the metric property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metric property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetric().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetricType }
     * 
     * 
     */
    public List<MetricType> getMetric() {
        if (metric == null) {
            metric = new ArrayList<MetricType>();
        }
        return this.metric;
    }

}
