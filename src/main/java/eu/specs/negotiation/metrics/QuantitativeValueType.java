
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for quantitativeValueType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="quantitativeValueType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Interval"/>
 *     &lt;enumeration value="Ratio"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "quantitativeValueType", namespace = "http://specs-project.eu/schemas/SLAtemplate")
@XmlEnum
public enum QuantitativeValueType {

    @XmlEnumValue("Interval")
    INTERVAL("Interval"),
    @XmlEnumValue("Ratio")
    RATIO("Ratio");
    private final String value;

    QuantitativeValueType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QuantitativeValueType fromValue(String v) {
        for (QuantitativeValueType c: QuantitativeValueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
