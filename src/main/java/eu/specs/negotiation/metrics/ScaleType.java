
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for scaleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scaleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Qualitative" type="{http://www.specs-project.eu/schemas/metrics}qualitativeValueType"/>
 *         &lt;element name="Quantitative" type="{http://www.specs-project.eu/schemas/metrics}quantitativeValueType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scaleType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "qualitative",
    "quantitative"
})
public class ScaleType {

    @XmlElement(name = "Qualitative")
    @XmlSchemaType(name = "string")
    protected QualitativeValueType qualitative;
    @XmlElement(name = "Quantitative")
    @XmlSchemaType(name = "string")
    protected QuantitativeValueType quantitative;

    /**
     * Gets the value of the qualitative property.
     * 
     * @return
     *     possible object is
     *     {@link QualitativeValueType }
     *     
     */
    public QualitativeValueType getQualitative() {
        return qualitative;
    }

    /**
     * Sets the value of the qualitative property.
     * 
     * @param value
     *     allowed object is
     *     {@link QualitativeValueType }
     *     
     */
    public void setQualitative(QualitativeValueType value) {
        this.qualitative = value;
    }

    /**
     * Gets the value of the quantitative property.
     * 
     * @return
     *     possible object is
     *     {@link QuantitativeValueType }
     *     
     */
    public QuantitativeValueType getQuantitative() {
        return quantitative;
    }

    /**
     * Sets the value of the quantitative property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantitativeValueType }
     *     
     */
    public void setQuantitative(QuantitativeValueType value) {
        this.quantitative = value;
    }

}
