
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for intervalType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="intervalType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="intervalItemsType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="integer"/>
 *               &lt;enumeration value="float"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="intervalItemStart" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *         &lt;element name="intervalItemStop" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *         &lt;element name="intervalItemStep" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "intervalType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "intervalItemsType",
    "intervalItemStart",
    "intervalItemStop",
    "intervalItemStep"
})
public class IntervalType {

    @XmlElement(required = true, namespace = "")
    protected String intervalItemsType;
    @XmlElement(required = true, namespace = "")
    @XmlSchemaType(name = "anySimpleType")
    protected String intervalItemStart;
    @XmlElement(required = true, namespace = "")
    @XmlSchemaType(name = "anySimpleType")
    protected String intervalItemStop;
    @XmlElement(required = true, namespace = "")
    @XmlSchemaType(name = "anySimpleType")
    protected String intervalItemStep;

    /**
     * Gets the value of the intervalItemsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntervalItemsType() {
        return intervalItemsType;
    }

    /**
     * Sets the value of the intervalItemsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntervalItemsType(String value) {
        this.intervalItemsType = value;
    }

    /**
     * Gets the value of the intervalItemStart property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIntervalItemStart() {
        return intervalItemStart;
    }

    /**
     * Sets the value of the intervalItemStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIntervalItemStart(String value) {
        this.intervalItemStart = value;
    }

    /**
     * Gets the value of the intervalItemStop property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIntervalItemStop() {
        return intervalItemStop;
    }

    /**
     * Sets the value of the intervalItemStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIntervalItemStop(String value) {
        this.intervalItemStop = value;
    }

    /**
     * Gets the value of the intervalItemStep property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIntervalItemStep() {
        return intervalItemStep;
    }

    /**
     * Sets the value of the intervalItemStep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIntervalItemStep(String value) {
        this.intervalItemStep = value;
    }

}
