
package eu.specs.negotiation.metrics;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for AbstractMetricType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractMetricType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.specs-project.eu/schemas/metrics}referenceType">
 *       &lt;sequence>
 *         &lt;element name="AbstractMetricDefinition">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="unit" type="{http://www.specs-project.eu/schemas/metrics}unitType"/>
 *                   &lt;element name="scale" type="{http://www.specs-project.eu/schemas/metrics}scaleType"/>
 *                   &lt;element name="expression" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="definition" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AbstractMetricRuleDefinition" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RuleDefinition" type="{http://www.specs-project.eu/schemas/metrics}ruleDefinitionType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AbstractMetricParameterDefinition" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ParameterDefinition" type="{http://www.specs-project.eu/schemas/metrics}parameterDefinitionType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UnderlyingAbstractMetric" type="{http://www.specs-project.eu/schemas/metrics}referenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMetricType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "abstractMetricDefinition",
    "abstractMetricRuleDefinition",
    "abstractMetricParameterDefinition",
        "primaryAbstractMetric",
        "metricRules",
        "metricParameters",
        "note"
    //"underlyingAbstractMetric"
})
public class AbstractMetricType
    extends ReferenceType
{

    @XmlElement(name = "MetricDefinition", required = true)
    protected AbstractMetricType.AbstractMetricDefinition abstractMetricDefinition;
    @XmlElement(name = "AbstractMetricRuleDefinition")
    protected List<AbstractMetricType.AbstractMetricRuleDefinition> abstractMetricRuleDefinition;
    @XmlElement(name = "AbstractMetricParameterDefinition")
    protected List<AbstractMetricType.AbstractMetricParameterDefinition> abstractMetricParameterDefinition;

    @XmlElement(name = "note")
    protected String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPrimaryAbstractMetric() {
        return primaryAbstractMetric;
    }

    public void setPrimaryAbstractMetric(String primaryAbstractMetric) {
        this.primaryAbstractMetric = primaryAbstractMetric;
    }

    @XmlElement(name = "PrimaryAbstractMetric", required = true)
    protected String primaryAbstractMetric;

    public List<MetricType.MetricRules> getMetricRules() {
        if (metricRules == null)
            metricRules = new ArrayList<>();
        return metricRules;
    }

    public void setMetricRules(List<MetricType.MetricRules> metricRules) {
        this.metricRules = metricRules;
    }

    @XmlElement(name = "MetricRules")
    protected List<MetricType.MetricRules> metricRules;

    public List<MetricType.MetricParameters> getMetricParameters() {
        if (metricParameters == null)
            metricParameters = new ArrayList<>();
        return metricParameters;
    }

    public void setMetricParameters(List<MetricType.MetricParameters> metricParameters) {
        this.metricParameters = metricParameters;
    }

    @XmlElement(name = "MetricParameters")
    protected List<MetricType.MetricParameters> metricParameters;

    /**
     * Gets the value of the abstractMetricDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractMetricType.AbstractMetricDefinition }
     *     
     */
    public AbstractMetricType.AbstractMetricDefinition getAbstractMetricDefinition() {
        return abstractMetricDefinition;
    }

    /**
     * Sets the value of the abstractMetricDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractMetricType.AbstractMetricDefinition }
     *     
     */
    public void setAbstractMetricDefinition(AbstractMetricType.AbstractMetricDefinition value) {
        this.abstractMetricDefinition = value;
    }

    /**
     * Gets the value of the abstractMetricRuleDefinition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the abstractMetricRuleDefinition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAbstractMetricRuleDefinition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractMetricType.AbstractMetricRuleDefinition }
     * 
     * 
     */
    public List<AbstractMetricType.AbstractMetricRuleDefinition> getAbstractMetricRuleDefinition() {
        if (abstractMetricRuleDefinition == null) {
            abstractMetricRuleDefinition = new ArrayList<AbstractMetricType.AbstractMetricRuleDefinition>();
        }
        return this.abstractMetricRuleDefinition;
    }

    /**
     * Gets the value of the abstractMetricParameterDefinition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the abstractMetricParameterDefinition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAbstractMetricParameterDefinition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractMetricType.AbstractMetricParameterDefinition }
     * 
     * 
     */
    public List<AbstractMetricType.AbstractMetricParameterDefinition> getAbstractMetricParameterDefinition() {
        if (abstractMetricParameterDefinition == null) {
            abstractMetricParameterDefinition = new ArrayList<AbstractMetricType.AbstractMetricParameterDefinition>();
        }
        return this.abstractMetricParameterDefinition;
    }

    /**
     * Gets the value of the underlyingAbstractMetric property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the underlyingAbstractMetric property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnderlyingAbstractMetric().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferenceType }
     * 
     * 
     */
//    public List<ReferenceType> getUnderlyingAbstractMetric() {
//        if (underlyingAbstractMetric == null) {
//            underlyingAbstractMetric = new ArrayList<ReferenceType>();
//        }
//        return this.underlyingAbstractMetric;
//    }




    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="unit" type="{http://www.specs-project.eu/schemas/metrics}unitType"/>
     *         &lt;element name="scale" type="{http://www.specs-project.eu/schemas/metrics}scaleType"/>
     *         &lt;element name="expression" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="definition" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "MetricDefinition", propOrder = {
        "unit",
        "scale",
        "expression",
        "definition",
        "note"
    })
    @XmlRootElement
    public static class AbstractMetricDefinition {

        @XmlElement(required = true)
        protected UnitType unit;
        @XmlElement(required = true)
        protected ScaleType scale;
        @XmlElement(required = true)
        protected String expression;
        @XmlElement(required = true)
        protected String definition;
        @XmlElement(required = true)
        protected String note;

        /**
         * Gets the value of the unit property.
         * 
         * @return
         *     possible object is
         *     {@link UnitType }
         *     
         */
        public UnitType getUnit() {
            return unit;
        }

        /**
         * Sets the value of the unit property.
         * 
         * @param value
         *     allowed object is
         *     {@link UnitType }
         *     
         */
        public void setUnit(UnitType value) {
            this.unit = value;
        }

        /**
         * Gets the value of the scale property.
         * 
         * @return
         *     possible object is
         *     {@link ScaleType }
         *     
         */
        public ScaleType getScale() {
            return scale;
        }

        /**
         * Sets the value of the scale property.
         * 
         * @param value
         *     allowed object is
         *     {@link ScaleType }
         *     
         */
        public void setScale(ScaleType value) {
            this.scale = value;
        }

        /**
         * Gets the value of the expression property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpression() {
            return expression;
        }

        /**
         * Sets the value of the expression property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpression(String value) {
            this.expression = value;
        }

        /**
         * Gets the value of the definition property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefinition() {
            return definition;
        }

        /**
         * Sets the value of the definition property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefinition(String value) {
            this.definition = value;
        }

        /**
         * Gets the value of the note property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNote() {
            return note;
        }

        /**
         * Sets the value of the note property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNote(String value) {
            this.note = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ParameterDefinition" type="{http://www.specs-project.eu/schemas/metrics}parameterDefinitionType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "parameterDefinition"
    })
    @XmlRootElement
    public static class AbstractMetricParameterDefinition {

        @XmlElement(name = "ParameterDefinition", required = true)
        protected ParameterDefinitionType parameterDefinition;

        /**
         * Gets the value of the parameterDefinition property.
         * 
         * @return
         *     possible object is
         *     {@link ParameterDefinitionType }
         *     
         */
        public ParameterDefinitionType getParameterDefinition() {
            return parameterDefinition;
        }

        /**
         * Sets the value of the parameterDefinition property.
         * 
         * @param value
         *     allowed object is
         *     {@link ParameterDefinitionType }
         *     
         */
        public void setParameterDefinition(ParameterDefinitionType value) {
            this.parameterDefinition = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RuleDefinition" type="{http://www.specs-project.eu/schemas/metrics}ruleDefinitionType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "AbstractMetricRuleDefinition", propOrder = {
        "ruleDefinition"
    })
    @XmlRootElement
    public static class AbstractMetricRuleDefinition {

        @XmlElement(name = "RuleDefinition", required = true)
        protected RuleDefinitionType ruleDefinition;

        /**
         * Gets the value of the ruleDefinition property.
         * 
         * @return
         *     possible object is
         *     {@link RuleDefinitionType }
         *     
         */
        public RuleDefinitionType getRuleDefinition() {
            return ruleDefinition;
        }

        /**
         * Sets the value of the ruleDefinition property.
         * 
         * @param value
         *     allowed object is
         *     {@link RuleDefinitionType }
         *     
         */
        public void setRuleDefinition(RuleDefinitionType value) {
            this.ruleDefinition = value;
        }

    }


}
