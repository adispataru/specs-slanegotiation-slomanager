/**
 * Created by adispataru on 7/5/15.
 */
@XmlSchema(
        namespace = "http://specs-project.eu/schemas/SLAtemplate",
        elementFormDefault = XmlNsForm.QUALIFIED)
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;