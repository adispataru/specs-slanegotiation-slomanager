
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for qualitativeValueType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="qualitativeValueType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nominal"/>
 *     &lt;enumeration value="Ordinal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "qualitativeValueType", namespace = "http://specs-project.eu/schemas/SLAtemplate")
@XmlEnum
public enum QualitativeValueType {

    @XmlEnumValue("Nominal")
    NOMINAL("Nominal"),
    @XmlEnumValue("Ordinal")
    ORDINAL("Ordinal");
    private final String value;

    QualitativeValueType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QualitativeValueType fromValue(String v) {
        for (QualitativeValueType c: QualitativeValueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
