
package eu.specs.negotiation.metrics;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for MetricType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetricType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.specs-project.eu/schemas/metrics}referenceType">
 *       &lt;sequence>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrimaryAbstractMetric" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MetricRules" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MetricRule" type="{http://www.specs-project.eu/schemas/metrics}metricRuleType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MetricParameters" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MetricParameter" type="{http://www.specs-project.eu/schemas/metrics}metricParameterType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Metric", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "note",
    "primaryAbstractMetric",
    "metricRules",
    "metricParameters"
})
public class MetricType
    extends ReferenceType
{

    @XmlElement(required = true)
    protected String note;
    @XmlElement(name = "PrimaryAbstractMetric", required = true)
    protected String primaryAbstractMetric;
    @XmlElement(name = "MetricRules")
    protected List<MetricType.MetricRules> metricRules;
    @XmlElement(name = "MetricParameters")
    protected List<MetricType.MetricParameters> metricParameters;

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the primaryAbstractMetric property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryAbstractMetric() {
        return primaryAbstractMetric;
    }

    /**
     * Sets the value of the primaryAbstractMetric property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryAbstractMetric(String value) {
        this.primaryAbstractMetric = value;
    }

    /**
     * Gets the value of the metricRules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metricRules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetricRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetricType.MetricRules }
     * 
     * 
     */
    public List<MetricType.MetricRules> getMetricRules() {
        if (metricRules == null) {
            metricRules = new ArrayList<MetricType.MetricRules>();
        }
        return this.metricRules;
    }

    /**
     * Gets the value of the metricParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metricParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetricParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetricType.MetricParameters }
     * 
     * 
     */
    public List<MetricType.MetricParameters> getMetricParameters() {
        if (metricParameters == null) {
            metricParameters = new ArrayList<MetricType.MetricParameters>();
        }
        return this.metricParameters;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MetricParameter" type="{http://www.specs-project.eu/schemas/metrics}metricParameterType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "metricParameter"
    })
    public static class MetricParameters {

        @XmlElement(name = "MetricParameter", required = true)
        protected MetricParameterType metricParameter;

        /**
         * Gets the value of the metricParameter property.
         * 
         * @return
         *     possible object is
         *     {@link MetricParameterType }
         *     
         */
        public MetricParameterType getMetricParameter() {
            return metricParameter;
        }

        /**
         * Sets the value of the metricParameter property.
         * 
         * @param value
         *     allowed object is
         *     {@link MetricParameterType }
         *     
         */
        public void setMetricParameter(MetricParameterType value) {
            this.metricParameter = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MetricRule" type="{http://www.specs-project.eu/schemas/metrics}metricRuleType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "metricRule"
    })
    public static class MetricRules extends ReferenceType {

        @XmlElement(name = "MetricRule", required = true)
        protected MetricRuleType metricRule;

        /**
         * Gets the value of the metricRule property.
         * 
         * @return
         *     possible object is
         *     {@link MetricRuleType }
         *     
         */
        public MetricRuleType getMetricRule() {
            return metricRule;
        }

        /**
         * Sets the value of the metricRule property.
         * 
         * @param value
         *     allowed object is
         *     {@link MetricRuleType }
         *     
         */
        public void setMetricRule(MetricRuleType value) {
            this.metricRule = value;
        }

    }

}
