
package eu.specs.negotiation.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unitType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="enumUnit" type="{http://www.specs-project.eu/schemas/metrics}enumType"/>
 *         &lt;element name="intervalUnit" type="{http://www.specs-project.eu/schemas/metrics}intervalType"/>
 *       &lt;/choice>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unitType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "enumUnit",
    "intervalUnit"
})
public class UnitType {

    protected EnumType enumUnit;
    protected IntervalType intervalUnit;
    @XmlAttribute(name = "name", required = true)
    protected String name;

    /**
     * Gets the value of the enumUnit property.
     * 
     * @return
     *     possible object is
     *     {@link EnumType }
     *     
     */
    public EnumType getEnumUnit() {
        return enumUnit;
    }

    /**
     * Sets the value of the enumUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumType }
     *     
     */
    public void setEnumUnit(EnumType value) {
        this.enumUnit = value;
    }

    /**
     * Gets the value of the intervalUnit property.
     * 
     * @return
     *     possible object is
     *     {@link IntervalType }
     *     
     */
    public IntervalType getIntervalUnit() {
        return intervalUnit;
    }

    /**
     * Sets the value of the intervalUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntervalType }
     *     
     */
    public void setIntervalUnit(IntervalType value) {
        this.intervalUnit = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
