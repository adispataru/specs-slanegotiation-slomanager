
package eu.specs.negotiation.metrics;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.metrics package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityMetrics_QNAME = new QName("http://specs-project.eu/schemas/SLAtemplate", "security_metrics");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.metrics
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnumType }
     * 
     */
    public EnumType createEnumType() {
        return new EnumType();
    }

    /**
     * Create an instance of {@link MetricType }
     * 
     */
    public MetricType createMetricType() {
        return new MetricType();
    }

    /**
     * Create an instance of {@link AbstractMetricType }
     * 
     */
    public AbstractMetricType createAbstractMetricType() {
        return new AbstractMetricType();
    }

    /**
     * Create an instance of {@link SecurityMetricsType }
     * 
     */
    public SecurityMetricsType createSecurityMetricsType() {
        return new SecurityMetricsType();
    }

    /**
     * Create an instance of {@link ReferenceType }
     * 
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create an instance of {@link MetricRuleType }
     * 
     */
    public MetricRuleType createMetricRuleType() {
        return new MetricRuleType();
    }

    /**
     * Create an instance of {@link UnitType }
     * 
     */
    public UnitType createUnitType() {
        return new UnitType();
    }

    /**
     * Create an instance of {@link IntervalType }
     * 
     */
    public IntervalType createIntervalType() {
        return new IntervalType();
    }

    /**
     * Create an instance of {@link RuleDefinitionType }
     * 
     */
    public RuleDefinitionType createRuleDefinitionType() {
        return new RuleDefinitionType();
    }

    /**
     * Create an instance of {@link ScaleType }
     * 
     */
    public ScaleType createScaleType() {
        return new ScaleType();
    }

    /**
     * Create an instance of {@link ParameterDefinitionType }
     * 
     */
    public ParameterDefinitionType createParameterDefinitionType() {
        return new ParameterDefinitionType();
    }

    /**
     * Create an instance of {@link MetricParameterType }
     * 
     */
    public MetricParameterType createMetricParameterType() {
        return new MetricParameterType();
    }

    /**
     * Create an instance of {@link EnumType.EnumItems }
     * 
     */
    public EnumType.EnumItems createEnumTypeEnumItems() {
        return new EnumType.EnumItems();
    }

    /**
     * Create an instance of {@link MetricType.MetricRules }
     * 
     */
    public MetricType.MetricRules createMetricTypeMetricRules() {
        return new MetricType.MetricRules();
    }

    /**
     * Create an instance of {@link MetricType.MetricParameters }
     * 
     */
    public MetricType.MetricParameters createMetricTypeMetricParameters() {
        return new MetricType.MetricParameters();
    }

    /**
     * Create an instance of {@link AbstractMetricType.AbstractMetricDefinition }
     * 
     */
    public AbstractMetricType.AbstractMetricDefinition createAbstractMetricTypeAbstractMetricDefinition() {
        return new AbstractMetricType.AbstractMetricDefinition();
    }

    /**
     * Create an instance of {@link AbstractMetricType.AbstractMetricRuleDefinition }
     * 
     */
    public AbstractMetricType.AbstractMetricRuleDefinition createAbstractMetricTypeAbstractMetricRuleDefinition() {
        return new AbstractMetricType.AbstractMetricRuleDefinition();
    }

    /**
     * Create an instance of {@link AbstractMetricType.AbstractMetricParameterDefinition }
     * 
     */
    public AbstractMetricType.AbstractMetricParameterDefinition createAbstractMetricTypeAbstractMetricParameterDefinition() {
        return new AbstractMetricType.AbstractMetricParameterDefinition();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SecurityMetricsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.eu/schemas/SLAtemplate", name = "security_metrics")
    public JAXBElement<SecurityMetricsType> createSecurityMetrics(SecurityMetricsType value) {
        return new JAXBElement<SecurityMetricsType>(_SecurityMetrics_QNAME, SecurityMetricsType.class, null, value);
    }

}
