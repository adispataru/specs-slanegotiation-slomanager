
package eu.specs.negotiation.metrics;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enumItemsType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="integer"/>
 *               &lt;enumeration value="float"/>
 *               &lt;enumeration value="string"/>
 *               &lt;enumeration value="boolean"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="enumItems">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="enumItem" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enumType", namespace = "http://specs-project.eu/schemas/SLAtemplate", propOrder = {
    "enumItemsType",
    "enumItems"
})
public class EnumType {

    @XmlElement(required = true)
    protected String enumItemsType;
    @XmlElement(required = true)
    protected EnumType.EnumItems enumItems;

    /**
     * Gets the value of the enumItemsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumItemsType() {
        return enumItemsType;
    }

    /**
     * Sets the value of the enumItemsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumItemsType(String value) {
        this.enumItemsType = value;
    }

    /**
     * Gets the value of the enumItems property.
     * 
     * @return
     *     possible object is
     *     {@link EnumType.EnumItems }
     *     
     */
    public EnumType.EnumItems getEnumItems() {
        return enumItems;
    }

    /**
     * Sets the value of the enumItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumType.EnumItems }
     *     
     */
    public void setEnumItems(EnumType.EnumItems value) {
        this.enumItems = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="enumItem" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enumItem"
    })
    public static class EnumItems {

        @XmlElement(required = true)
        protected List<String> enumItem;

        /**
         * Gets the value of the enumItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the enumItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnumItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getEnumItem() {
            if (enumItem == null) {
                enumItem = new ArrayList<String>();
            }
            return this.enumItem;
        }

    }

}
